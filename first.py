import logging

logging.basicConfig(format='%(message)s', level=logging.DEBUG)


def type_args(x, y, *args, **kwargs):
    logging.info('This is a function that receives any number of parameters and returns\n'
                 'a sorted dictionary with all the parameters in categories of its own type')
    dict1 = {0: {'x': x}, 1: {'y': y}}
    dict2 = {}
    for i in range(len(args)):
        dict1[i + 2] = {'arg' + str(i): args[i]}
    for kw, arg in kwargs.items():
        i = i + 1
        dict1[str(i)] = {kw: arg}
    for k, v in dict1.items():
        for k_inv, v_inv in v.items():
            name = v_inv.__class__.__name__
            if name in dict2:
                dict2[name].update(v)
            else:
                dict2[name] = v
    return dict2


if __name__ == '__main__':
    for item in type_args('t', 4, 3, [0, 3], 6 + 5j, 5, 7,t=8, b=9).items():
        print(*item)
